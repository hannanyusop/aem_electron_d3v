[![AEM Enersol](http://i0.wp.com/aemenersol.com/wp-content/uploads/2015/12/Logo-AEM-for-MegaProject-Final.png?fit=290%2C129)](http://aemenersol.com)

AEM Enersol is an independent integrated consultancy services, from upstream to downstream. Our impartiality allows us to provide a high quality advise to optimize clients' portfolio in a business. Our principle is grounded in an ultimate priority - achieving clients' needs at beyond the best limit.

[![Electron Logo](https://electronjs.org/images/electron-logo.svg)](https://electronjs.org)

# Electron Interview Test

If you can build a website, you can build a desktop app. Electron is a framework for creating native applications with web technologies like JavaScript, HTML, and CSS. It takes care of the hard parts so you can focus on the core of your application.

## Guideline

You will be given **2 Days** to complete the test. You may use whatever resources you like as long as you are following the below **Requirements**.

## Requirements
- Create an [Electron](https://www.electronjs.org/) desktop application
- User is required to login before he is able to open the application:
  - Consume the login API from the previous test. If the login is invalid,
  - Validate the credentials entered by the user through [PouchDB](https://pouchdb.com/)


## Login Credentials

Local Database
- username : ** local@aemenersol.com **
- password : ** Test@123 **

Remote Database
- username : ** user@aemenersol.com **
- password : ** Test@123 **