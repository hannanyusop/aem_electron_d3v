
export interface UserResponseModel{
  _id: number
  firstName: string;
  lastName: string;
  username: string;
  password: string;
}
