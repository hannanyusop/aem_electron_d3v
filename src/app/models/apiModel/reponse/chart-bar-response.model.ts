
export interface ChartBarResponseModel {
  name: string;
  value: number;
}
