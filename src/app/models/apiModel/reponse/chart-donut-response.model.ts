
export interface ChartDonutResponseModel{
  name: string;
  value: number;
}
