import { Injectable, EventEmitter } from "@angular/core";

import PouchDB from "pouchdb";
import {UserResponseModel} from "../models/apiModel/reponse/user-response.model";
import {SignInRequestModel} from "../models/apiModel/request/sign-in-request-model";

@Injectable({
    providedIn: "root"
})
export class DataService {
    private db: any;
    private isInstantiated: boolean;
    private listener: EventEmitter<any> = new EventEmitter();
    
    

    public constructor() {
        
        if (!this.isInstantiated) {
            this.db = new PouchDB("aem_backup");
            this.isInstantiated = true;

            let doc  = {
                _id: 'user',
                firstName: 'Raju',
                lastName: 'Kumar',
                username: 'local@aemenersol.com',
                password: 'Test@123'
            }
            this.db.put(doc, function(err, response) {
                if (err) {
                    return console.log(err);
                } else {
                    console.log("Document created Successfully");
                }
            });
        }
    }

    async login(body : SignInRequestModel): Promise<UserResponseModel | null>{

        return this.db.get("user").then(function (doc) : UserResponseModel | null {
            
            if (doc.password == body.password && doc.username == body.username) {
                return doc;
            } else {
                return null;
            }
        }).catch(function (err) {
            console.log(err);
            return null;
        });
    }

    public get(id: string) {
        return this.db.get(id);
    }
}
