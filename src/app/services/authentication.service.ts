import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {SignInRequestModel} from "../models/apiModel/request/sign-in-request-model";

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private tokenKey = 'token';

  constructor(
    private router: Router,
    private http: HttpClient
  ) {}

  public login(body : SignInRequestModel): Observable<string> {
    return this.http.post(
      environment.apiUrl + 'account/login',
      body,
      { responseType: 'text' }
    );
  }

  public logout() {
    localStorage.removeItem(this.tokenKey);
    this.router.navigate(['/login']);
  }

  public isLoggedIn(): boolean {
    let token = localStorage.getItem(this.tokenKey);
    return token != null && token.length > 0;
  }

  public getToken(): string | null {
    return this.isLoggedIn() ? localStorage.getItem(this.tokenKey) : null;
  }
}
