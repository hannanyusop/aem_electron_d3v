import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  constructor(private http: HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    // withCredentials: true,
  };

  public getData(): Observable<any> {

    return this.http.get<any>( environment.apiUrl + 'dashboard', {...this.httpOptions,})
      .pipe((r: any) => { return r }, (e: any) => { return e }
    )
  }

}
