import { Component } from '@angular/core';
import {FormGroup, Validators, FormBuilder} from '@angular/forms';

import {AuthenticationService} from "../../services/authentication.service";
import {Router} from "@angular/router";
import {SignInRequestModel} from "../../models/apiModel/request/sign-in-request-model";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent {
  private messageError : string | null = null;
  private tokenKey = 'token';
  public  loginButtonDisabled : boolean = false;

  public validation_messages = {
    //funderInfo
    username: [
      {type: 'required', message: 'Email is required'},
      {type: 'email', message: 'Email is not valid'},
      {type: 'minlength', message: 'Email minimum 4 character'},
    ],
    password: [
      {type: 'required', message: 'Password is required'},
      {type: 'minlength', message: 'Password minimum 4 character'},
    ]
  }

  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService : AuthenticationService,
    private router: Router,
    
    private dataService: DataService
  ) {
    
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email, Validators.minLength(4)]],
      password: ['', [Validators.required, Validators.minLength(4)]],
    });

    this.loginForm.patchValue({
      username: 'local@aemenersol.com',
      password: 'Test@123'
    })
  }

  ngOnInit(): void {
  }

  onSubmit() {

    this.loginButtonDisabled = true;

    let body : SignInRequestModel = this.loginForm.value;
    
    this.authService.login(body)
      .subscribe({
        next: (token) => {
          token = token.replace(/^"(.*)"$/, '$1');
          localStorage.setItem(this.tokenKey, token);
          this.router.navigate(['/auth/dashboard']);
          return true;
        },
        error: (err) => {

          this.dataService.login(body).then(data => {

            this.loginButtonDisabled = false;

            if(data == null){
              this.messageError = "Invalid Credentials";
              return false;
            }
            
            this.loginButtonDisabled = false;
            localStorage.setItem(this.tokenKey, JSON.stringify(data));
            this.router.navigate(['/auth/dashboard']);
            return true;

          }).catch(err => {
            this.loginButtonDisabled = false;
            this.messageError = "Ops! Something went wrong. Please try again later.";
          })
        },
        complete: () => {
          this.loginButtonDisabled = false;
        }
      });

  }

  getClass(formControlName: string) {
    return {
      'is-valid': this.loginForm.get(formControlName)?.valid,
      'is-invalid':
        this.loginForm.get(formControlName)?.invalid &&
        (this.loginForm.get(formControlName)?.dirty ||
          this.loginForm.get(formControlName)?.touched)
    };
  }

}
