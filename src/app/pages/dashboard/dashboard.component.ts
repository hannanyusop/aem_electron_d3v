import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import {DashboardService} from "../../services/user/dashboard.service";
import {DashboardResponseModel} from "../../models/apiModel/reponse/dashboard-response.model";
import {UserResponseModel} from "../../models/apiModel/reponse/user-response.model";
import {ChartBarResponseModel} from "../../models/apiModel/reponse/chart-bar-response.model";
import {ChartDonutResponseModel} from "../../models/apiModel/reponse/chart-donut-response.model";
import {AuthenticationService} from "../../services/authentication.service";
import {Router} from "@angular/router";

am4core.useTheme(am4themes_animated);

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  private donutChart!: am4charts.PieChart;
  private barChart!: am4charts.XYChart;

  userData : UserResponseModel[] = [];
  headers  : string[] = ['#', 'First Name', 'Last Name', 'Username'];

  constructor(
    private authService : AuthenticationService,
    private dashboardService: DashboardService,
    private zone: NgZone,
    
    private router: Router,
  ) { }

  ngOnInit() {



    this.dashboardService.getData().subscribe({
      next: (data: DashboardResponseModel) => {

          this.userData = data.tableUsers;
          this.zone.runOutsideAngular(() => {
          this.createBarChart(data.chartBar);
          this.createDonutChart(data.chartDonut);
        });

        },
      error: (error: any) => {
      }
    });
  }

  ngOnDestroy() {

    if (this.donutChart) this.donutChart.dispose();
    if (this.barChart) this.barChart.dispose();
  }

  createDonutChart(data : ChartDonutResponseModel[]) {

    this.donutChart = am4core.create("donutChart", am4charts.PieChart);
    this.donutChart.data = data;
    const pieSeries = this.donutChart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = 'value';
    pieSeries.dataFields.category = 'name';
    pieSeries.innerRadius = am4core.percent(50);
    this.donutChart.legend = new am4charts.Legend();
  }

  createBarChart(data : ChartBarResponseModel[]) {

    am4core.useTheme(am4themes_animated);
    const chart = am4core.create("barChart", am4charts.XYChart);

    chart.data = data;

    // Category Axis
    const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "name";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 30;

    const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

    // Series
    const series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = "value";
    series.dataFields.categoryX = "name";
    series.name = "Values";
    series.columns.template.tooltipText = "{categoryX}: {valueY}";
    series.columns.template.fill = am4core.color("#104547");
    series.columns.template.stroke = am4core.color("#104547");

    this.barChart = chart;
  }
  
  logout() {
    this.authService.logout();
    this.router.navigate(['/sign-in']);
  }

}
