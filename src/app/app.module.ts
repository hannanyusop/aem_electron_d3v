import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { TokenInterceptor } from './helpers/token.interceptor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignInComponent} from "./pages/sign-in/sign-in.component";
import {DashboardComponent} from "./pages/dashboard/dashboard.component";
import { PanelComponent } from './components/panel/panel.component';
import { LoadingBarComponent } from './components/loading-bar/loading-bar.component';
import {TableComponent} from "./components/table/table.component";

@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    DashboardComponent,
    PanelComponent,
    TableComponent,
    LoadingBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
