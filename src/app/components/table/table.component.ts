import { Component } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  inputs: [
    'headers',
    'data',
  ]
})
export class TableComponent {

  headers : string[] = [];
  data    : any[] = [];

}
